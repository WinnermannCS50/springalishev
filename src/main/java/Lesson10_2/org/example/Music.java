package Lesson10_2.org.example;

/**
 * Все жанры музыки будут реализовывать интерфейс Music
 */
public interface Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    String getSong();
}
