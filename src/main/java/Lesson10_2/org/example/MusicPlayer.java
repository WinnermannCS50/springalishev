package Lesson10_2.org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * class MusicPlayer зависит от музыки
 *
 * @Component - создает Бин класса MusicPlayer
 */
@Component
public class MusicPlayer {


    //поле интерфейс Music
    private Music music;

    /**
     * Инверсия управления
     */

    /**
     * Конструктор с параметром - жанр музыки
     * @param music - жанр музыки
     *
     * @Autowired - внедряет подходящий Бин в конструктор MusicPlayer
     */
    @Autowired
    public MusicPlayer(Music music) {
        this.music = music;
    }


    /**
     * В методе playMusic() будем получать песню
     */
    public void playMusic(){
        System.out.println("Playing: " + music.getSong());

    }
}
