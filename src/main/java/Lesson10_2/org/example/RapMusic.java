package Lesson10_2.org.example;

/**
 * class RapMusic реализует интерфейс Music
 */
public class RapMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        return "Beautiful Pain";
    }
}
