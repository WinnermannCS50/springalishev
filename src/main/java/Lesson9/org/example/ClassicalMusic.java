package Lesson9.org.example;

import org.springframework.stereotype.Component;

/**
 * class ClassicalMusic реализует интерфейс Music
 */
@Component
public class ClassicalMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        return "Hungarian Rhapsody";
    }
}
