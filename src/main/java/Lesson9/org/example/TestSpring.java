package Lesson9.org.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        /**
         * ClassPathXmlApplicationContext - это специальный класс Спринга
         * Доступ к классу появляется благодаря зависимости spring-context
         * Данный класс собирает все Бины из applicationContext.xml по их id и ложит в
         * context = new ClassPathXmlApplicationContext("applicationContext9.xml");
         */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext9.xml");

        /**
         * Зесь указываем id="musicBean" и имя класса(интерфейса) Бин которого мы хотим получить Music.class
         *
         * достаем Bean из applicationContext4.xml по его id="musicBean", вызываем через метод getBean()
         * используем два аргумента id="musicBean" и объект какого класса(интерфейса) хотим получить Music.class
         * Объект класса context.getBean("musicBean", Music.class); помещаем в переменную music
         */
        Music music = context.getBean("musicBean", Music.class);

        /**
         * Внедрение зависимостей вручную через конструктор
         * Внедряем Интерфейс music в Объект musicPlayer = new MusicPlayer()
         */
        MusicPlayer musicPlayer = new MusicPlayer(music);

        /**
         * После внедрения зависимости можно использовать musicPlayer
         */
        musicPlayer.playMusic();


        Music music2 = context.getBean("classicalMusic", Music.class);
        MusicPlayer classicalMisicPlayer = new MusicPlayer(music2);

        /**
         * После внедрения зависимости можно использовать classicalMisicPlayer
         */
        classicalMisicPlayer.playMusic();


        /**
         * закрыть context
         */
        context.close();
    }
}
