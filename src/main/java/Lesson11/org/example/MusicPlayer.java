package Lesson11.org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * class MusicPlayer зависит от музыки
 *
 * @Component - создает Бин класса MusicPlayer
 */
@Component
public class MusicPlayer {

    /**
     * поле интерфейс Music
     *
     * Внедрение зависимости при помощи Поля
     *
     * @Autowired - внедряет подходящий Бин в Поле private Music music
     *
     * @Qualifier("classicalMusic") - применяется если подходящих Бинов не один, внедрён будет Бин в скобках с id="classicalMusic"
     * @Qualifier - уточняет какой именно Бин будет внедрен в зависимость
     */
    @Autowired
    @Qualifier("classicalMusic")
    private Music music;

    /**
     * В методе playMusic() будем получать песню
     * @return
     */
    public String playMusic(){
        return "Playing: " + music.getSong();

    }
}
