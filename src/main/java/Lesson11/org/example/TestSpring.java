package Lesson11.org.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        /**
         * ClassPathXmlApplicationContext - это специальный класс Спринга
         * Доступ к классу появляется благодаря зависимости spring-context
         * Данный класс собирает все Бины из applicationContext.xml по их id и ложит в
         * context = new ClassPathXmlApplicationContext("applicationContext11.xml");
         */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext11.xml");

        /**
         * Зесь указываем id="musicPlayer" и имя класса(интерфейса) Бин которого мы хотим получить MusicPlayer.class
         *
         * достаем Bean из applicationContext10_6.xml по его id="computer", вызываем через метод getBean()
         * используем два аргумента id="computer" и объект какого класса(интерфейса) хотим получить Computer.class
         * Объект класса context.getBean("computer", Computer.class)
         */
        Computer computer = context.getBean("computer", Computer.class);
        System.out.println(computer);

        /**
         * закрыть context
         */
        context.close();
    }
}
