package Lesson7_2.org.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Пример паттерна Singleton
 * https://github.com/NeilAlishev/SpringCourse/blob/master/Lesson7Singleton.java
 */

public class TestSpring {
    public static void main(String[] args) {
        /**
         * ClassPathXmlApplicationContext - это специальный класс Спринга
         * Доступ к классу появляется благодаря зависимости spring-context
         * Данный класс собирает все Бины из applicationContext.xml по их id и ложит в
         * context = new ClassPathXmlApplicationContext("applicationContext7_2.xml");
         */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext7_2.xml");

        /**
         * Создание двух Бинов (Объектов) с применением Паттерна scope="prototype"
         */
        MusicPlayer firstMusicPlayer = context.getBean("musicPlayer", MusicPlayer.class);

        MusicPlayer secondMusicPlayer = context.getBean("musicPlayer", MusicPlayer.class);

        /**
         * Проверить указывают ли обе переменные (firstMusicPlayer и secondMusicPlayer) на один и тот же участок памяти
         * Проверить равны ли объекты
         * false - будет означать, что переменные указывают(ссылаются) на разные объекты
         * разный хэш - тоже подтверждает что обе переменные указывают(ссылаются) на разные объекты
         */
        boolean comparison = firstMusicPlayer == secondMusicPlayer;
        System.out.println(comparison);
        System.out.println(firstMusicPlayer);
        System.out.println(secondMusicPlayer);

        /**
         * Изменить значение переменной volume у плейера firstMusicPlayer
         * При установке параметра volume = 10 для firstMusicPlayer, параметр volume не меняется
         * для secondMusicPlayer
         * Это происходит потому, что обе переменные (firstMusicPlayer и secondMusicPlayer) ссылаются на разные объекты
         * Применяется паттерн Prototype
         * В файле applicationContext7_2.xml явным образом нужно указать scope="prototype"
         */
        firstMusicPlayer.setVolume(10);
        System.out.println(firstMusicPlayer.getVolume() + " volume для firstMusicPlayer");
        System.out.println(secondMusicPlayer.getVolume() + " volume для secondMusicPlayer");

        /**
         * закрыть context
         */
        context.close();
    }
}
