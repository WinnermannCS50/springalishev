package Lesson10_5.org.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        /**
         * ClassPathXmlApplicationContext - это специальный класс Спринга
         * Доступ к классу появляется благодаря зависимости spring-context
         * Данный класс собирает все Бины из applicationContext.xml по их id и ложит в
         * context = new ClassPathXmlApplicationContext("applicationContext10_5.xml");
         */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext10_5.xml");

        /**
         * Зесь указываем id="musicPlayer" и имя класса(интерфейса) Бин которого мы хотим получить MusicPlayer.class
         *
         * достаем Bean из applicationContext10_5.xml по его id="musicPlayer", вызываем через метод getBean()
         * используем два аргумента id="musicPlayer" и объект какого класса(интерфейса) хотим получить MusicPlayer.class
         * Объект класса context.getBean("musicPlayer", MusicPlayer.class); помещаем в переменную music
         */
        MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);

        /**
         * После внедрения зависимости можно использовать musicPlayer
         */
        musicPlayer.playMusic();

        /**
         * закрыть context
         */
        context.close();
    }
}
