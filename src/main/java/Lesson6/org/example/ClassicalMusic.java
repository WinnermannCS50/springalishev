package Lesson6.org.example;

/**
 * class ClassicalMusic реализует интерфейс Music
 */
public class ClassicalMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        return "Hungarian Rhapsody";
    }
}
