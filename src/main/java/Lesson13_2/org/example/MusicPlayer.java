package Lesson13_2.org.example;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

/**
 * class MusicPlayer зависит от музыки
 *
 * @Component - создает Бин класса MusicPlayer
 */
public class MusicPlayer {

    /**
     * @Value - внедряет знчение "Some name" в поле name из файла musicPlayer13_2.properties
     */
    @Value("${musicPlayer.name}")
    private String name;
    /**
     * @Value - внедряет знчение в поле volume из файла musicPlayer13_2.properties
     */
    @Value("${musicPlayer.volume}")
    private int volume;

    public String getName() {
        return name;
    }

    public int getVolume() {
        return volume;
    }

    /**
     * Конструктор с параметром - жанр музыки
     * @param music1 - жанр музыки RockMusic с id="musicBean"
     * @param music2 - жанр музыки ClassicalMusic с id="classicalMusic"
     *
     * Внедрение двух зависимостей при помощи Конструктора
     *
     * @Autowired - внедряет подходящий Бин в Поле private Music music1
     * @Autowired - внедряет подходящий Бин в Поле private Music music2
     *
     * @Qualifier("musicBean") - уточняет какой именно Бин будет внедрен в зависимость music1 через Конструктор
     * @Qualifier("classicalMusic") - уточняет какой именно Бин будет внедрен в зависимость music2 через Конструктор
     */
    private Music music1;
    private Music music2;

    public MusicPlayer(@Qualifier("musicBean") Music music1, @Qualifier("classicalMusic") Music music2) {
        this.music1 = music1;
        this.music2 = music2;
    }

    /**
     * В методе playMusic() будем получать песню
     * @return
     */
    public String playMusic(){
        return "Playing: " + music1.getSong() + ", " + music2.getSong();

    }
}
