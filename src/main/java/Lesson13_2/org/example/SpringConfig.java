package Lesson13_2.org.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Этот класс делает тоже самое, что и файл applicationContext.xml
 * можно использовать любой из них
 *
 * @Configuration - помечает класс SpringConfig как конфигурационный
 *
 * Создёт Бины классов помеченных аннотацией @Component
 * @ComponentScan("Lesson13.org.example") - тоже что <context:component-scan base-package="Lesson13.org.example"/>
 * в файле applicationContext.xml
 *
 * Позволяет применять настройки из файла musicPlayer13.properties
 * @PropertySource("classpath:musicPlayer13.properties") - тоже что <context:property-placeholder location="classpath:musicPlayer12_3.properties"/>
 * в файле applicationContext.xml
 *
 * @Bean - Создание объекта Bean ClassicalMusic
 * @Bean - Создание объекта Bean RockMusic
 * @Bean - Создание объекта Bean MusicPlayer
 * @Bean - Создание объекта Bean Computer
 */
@Configuration
@PropertySource("classpath:musicPlayer13_2.properties")
public class SpringConfig {
    @Bean
    public ClassicalMusic classicalMusic(){
        return new ClassicalMusic();
    }

    @Bean
    public RockMusic rockMusic(){
        return new RockMusic();
    }

    @Bean
    public MusicPlayer musicPlayer(){
        /**
         * В Бин MusicPlayer внедрены зависимости: classicalMusic(), rockMusic()
         */
        return new MusicPlayer(classicalMusic(), rockMusic());
    }

    @Bean
    public Computer computer(){
        /**
         * В Бин Computer внедрена зависимость: musicPlayer()
         */
        return new Computer(musicPlayer());
    }
}
