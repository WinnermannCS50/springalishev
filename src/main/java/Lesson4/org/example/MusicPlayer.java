package Lesson4.org.example;

/**
 * class MusicPlayer зависит от музыки
 */
public class MusicPlayer {


    //поле interface Music
    private Music music;

    /**
     * Инверсия управления
     */

    /**
     * Конструктор с параметром - жанр музыки
     * @param music - жанр музыки
     */
    public MusicPlayer(Music music) {
        this.music = music;
    }

    /**
     * В методе playMusic() будем получать песню
     */
    public void playMusic(){
        System.out.println("Playing: " + music.getSong());

    }
}
