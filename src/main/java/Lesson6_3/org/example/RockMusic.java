package Lesson6_3.org.example;

/**
 * class RockMusic реализует интерфейс Music
 */
public class RockMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        return "Wind cries Mary";
    }
}
