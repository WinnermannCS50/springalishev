package Lesson6_3.org.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        /**
         * ClassPathXmlApplicationContext - это специальный класс Спринга
         * Доступ к классу появляется благодаря зависимости spring-context
         * Данный класс собирает все Бины из applicationContext.xml по их id и ложит в
         * context = new ClassPathXmlApplicationContext("applicationContext6_3.xml");
         */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext6_3.xml");

        /**
         * Внедрение зависимостей вручную через Setter
         */
        MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);

        /**
         * После внедрения зависимости можно использовать musicPlayer
         */
        musicPlayer.playMusic();

        System.out.println(musicPlayer.getName());
        System.out.println(musicPlayer.getVolume());

        /**
         * закрыть context
         */
        context.close();
    }
}
