package Lesson6_3.org.example;

import java.util.ArrayList;
import java.util.List;

/**
 * class MusicPlayer зависит от музыки
 */
public class MusicPlayer {


    //поле interface Music
    //private Music music;
    List<Music> musicList = new ArrayList<>();

    //Название нашего плейера
    private String name;
    //Громкость
    private int volume;

    /**
     * Геттеры и Сеттеры для name и volume
     * @return
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    /**
     * Инверсия управления
     */

    /**
     * Пустой конструктор
     */
    public MusicPlayer() {
    }

    /**
     * Внедрение зависимости при помощи Сеттера
     * @param musicList
     */
    public void setMusicList(List<Music> musicList) {
        this.musicList = musicList;
    }

    /**
     * В методе playMusic() будем получать песню
     */
    public void playMusic(){
        for (Music music:musicList) {
            System.out.println("Playing: " + music.getSong());

        }

    }
}
