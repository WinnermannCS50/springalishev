package Lesson12_3.org.example;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * class ClassicalMusic реализует интерфейс Music
 *
 * @Component - создает Бин класса ClassicalMusic
 *
 * @Scope("prototype") - Бин ClassicalMusic будет со скоупом prototype
 */
@Component
public class ClassicalMusic implements Music {

    /**
     * @PostConstruct - вызывает Init-Method
     * Тоже самое что вызвать через файл init-method="doMyInit"
     */
    @PostConstruct
    public void doMyInit(){
        System.out.println("Doing my initialisation");
    }

    /**
     * @PreDestroy - вызывает Destroy-Method
     * Тоже самое что вызвать через файл destroy-method="doMyDestroy"
     */
    @PreDestroy
    public void doMyDestroy(){
        System.out.println("Doing my destruction");
    }
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        return "Hungarian Rhapsody";
    }
}
