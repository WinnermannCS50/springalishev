package Lesson5.org.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        /**
         * ClassPathXmlApplicationContext - это специальный класс Спринга
         * Доступ к классу появляется благодаря зависимости spring-context
         * Данный класс собирает все Бины из applicationContext.xml по их id и ложит в
         * context = new ClassPathXmlApplicationContext("applicationContext5.xml");
         */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext5.xml");

        /**
         * Внедрение зависимостей вручную через конструктор
         * Внедряем Интерфейс music в Объект musicPlayer через конструктор constructor-arg
         */

        MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);

        /**
         * После внедрения зависимости можно использовать musicPlayer
         */
        musicPlayer.playMusic();

        /**
         * закрыть context
         */
        context.close();
    }
}
