package Lesson11_3.org.example;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * class RockMusic реализует интерфейс Music
 *
 * @Component("musicBean") - этой аннотацией помечен класс Бин которого создаст Spring
 * musicBean - это id Бина, если в скобках id Бина не указать, то id будет имя класса с маленькой буквы
 * в нашем случае rockMusic
 */
@Component("musicBean")
public class RockMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        List<String> list = new ArrayList<>(3);
        list.add("Wind cries Mary");
        list.add("Moscow Calling");
        list.add("Show Must Go On");

        return list.get(new Random().nextInt(3));
    }
}
