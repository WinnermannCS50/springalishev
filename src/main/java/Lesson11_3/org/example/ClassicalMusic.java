package Lesson11_3.org.example;

import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * class ClassicalMusic реализует интерфейс Music
 *
 * @Component - создает Бин класса ClassicalMusic
 */
@Component
public class ClassicalMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        List<String> list = new LinkedList<>();
        list.add("Hungarian Rhapsody");
        list.add("Symphony no. 5 in C Minor, op. 67");
        list.add("Recviem");
        return list.get(new Random().nextInt(3));
    }
}
