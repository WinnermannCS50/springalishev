package Lesson2.org.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        /**
         * ClassPathXmlApplicationContext - это специальный класс Спринга
         * Доступ к классу появляется благодаря зависимости spring-context
         * Данный класс собирает все Бины из applicationContext.xml по их id и ложит в
         * context = new ClassPathXmlApplicationContext("applicationContext.xml");
         */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        /**
         * Зесь указываем id="testBean" и имя класса Бин которого мы хотим получить TestBean.class
         *
         * достаем Bean из applicationContext.xml по его id="testBean", вызываем через метод getBean()
         * используем два аргумента id="testBean" и объкт какого класса хотим получить TestBean.class
         * Объект класса context.getBean("testBean", TestBean.class); помещаем в переменную testBean
         */
        TestBean testBean = context.getBean("testBean", TestBean.class);

        /**
         * Вызовим имя(value="Kirill") getName() из объекта testBean
         */
        System.out.println(testBean.getName());

        /**
         * закрыть context
         */
        context.close();
    }
}
