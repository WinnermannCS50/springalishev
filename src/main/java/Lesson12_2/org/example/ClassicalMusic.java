package Lesson12_2.org.example;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * class ClassicalMusic реализует интерфейс Music
 *
 * @Component - создает Бин класса ClassicalMusic
 *
 * @Scope("prototype") - Бин ClassicalMusic будет со скоупом prototype
 */
@Component
@Scope("prototype")
public class ClassicalMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        return "Hungarian Rhapsody";
    }
}
