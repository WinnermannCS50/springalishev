package Lesson13.org.example;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Этот класс делает тоже самое, что и файл applicationContext.xml
 * можно использовать любой из них
 *
 * @Configuration - помечает класс SpringConfig как конфигурационный
 *
 * Создёт Бины классов помеченных аннотацией @Component
 * @ComponentScan("Lesson13.org.example") - тоже что <context:component-scan base-package="Lesson13.org.example"/>
 * в файле applicationContext.xml
 *
 * Позволяет применять настройки из файла musicPlayer13.properties
 * @PropertySource("classpath:musicPlayer13.properties") - тоже что <context:property-placeholder location="classpath:musicPlayer12_3.properties"/>
 * в файле applicationContext.xml
 */
@Configuration
@ComponentScan("Lesson13.org.example")
@PropertySource("classpath:musicPlayer13.properties")
public class SpringConfig {
}
