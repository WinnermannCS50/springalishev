package Lesson13_3.org.example;


/**
 * class RockMusic реализует интерфейс Music
 *
 * @Component("musicBean") - этой аннотацией помечен класс Бин которого создаст Spring
 * musicBean - это id Бина, если в скобках id Бина не указать, то id будет имя класса с маленькой буквы
 * в нашем случае rockMusic
 */
public class RockMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        return "Wind cries Mary";
    }
}
