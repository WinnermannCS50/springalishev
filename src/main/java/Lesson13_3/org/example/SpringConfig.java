package Lesson13_3.org.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

import java.util.Arrays;
import java.util.List;

/**
 * Этот класс делает тоже самое, что и файл applicationContext.xml
 * можно использовать любой из них
 *
 * @Configuration - помечает класс SpringConfig как конфигурационный
 *
 * Создёт Бины классов помеченных аннотацией @Component
 * @ComponentScan("Lesson13.org.example") - тоже что <context:component-scan base-package="Lesson13.org.example"/>
 * в файле applicationContext.xml
 *
 * Позволяет применять настройки из файла musicPlayer13.properties
 * @PropertySource("classpath:musicPlayer13.properties") - тоже что <context:property-placeholder location="classpath:musicPlayer12_3.properties"/>
 * в файле applicationContext.xml
 *
 * @Bean - Создание объекта Bean ClassicalMusic
 * @Bean - Создание объекта Bean RockMusic
 * @Bean - Создание объекта Bean RapMusic
 * @Bean - Создание объекта Bean MusicPlayer
 * @Bean - Создание объекта Bean Computer
 */
@Configuration
@PropertySource("classpath:musicPlayer13_3.properties")
public class SpringConfig {
    @Bean
    @Scope("prototype")
    public ClassicalMusic classicalMusic(){
        return new ClassicalMusic();
    }

    @Bean
    public RockMusic rockMusic(){
        return new RockMusic();
    }

    @Bean
    public RapMusic rapMusic(){
        return new RapMusic();
    }

    @Bean
    public List<Music> musicList() {
        // Этот лист неизменяемый (immutable)
        return Arrays.asList(classicalMusic(), rockMusic(), rapMusic());
    }

    @Bean
    public MusicPlayer musicPlayer(){
        /**
         * В Бин MusicPlayer внедрены зависимости: musicList()
         */
        return new MusicPlayer(musicList());
    }

    @Bean
    public Computer computer(){
        /**
         * В Бин Computer внедрена зависимость: musicPlayer()
         */
        return new Computer(musicPlayer());
    }
}
