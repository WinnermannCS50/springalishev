package Lesson13_3.org.example;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * class ClassicalMusic реализует интерфейс Music
 *
 * @Component - создает Бин класса ClassicalMusic
 *
 * @Scope("prototype") - Бин ClassicalMusic будет со скоупом prototype
 */
public class ClassicalMusic implements Music {

    /**
     * @PostConstruct - вызывает Init-Method
     * Тоже самое что вызвать init-method="doMyInit" через файл applicationContext.xml
     */
    @PostConstruct
    public void doMyInit(){
        System.out.println("Doing my initialisation");
    }

    /**
     * @PreDestroy - вызывает Destroy-Method
     * Тоже самое что вызвать destroy-method="doMyDestroy" через файл applicationContext.xml
     */
    @PreDestroy
    public void doMyDestroy(){
        System.out.println("Doing my destruction");
    }
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        return "Hungarian Rhapsody";
    }
}
