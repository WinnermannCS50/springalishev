package Lesson13_3.org.example;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * class MusicPlayer зависит от музыки
 *
 * @Component - создает Бин класса MusicPlayer
 */
public class MusicPlayer {

    /**
     * @Value - внедряет знчение "Some name" в поле name из файла musicPlayer13_3.properties
     */
    @Value("${musicPlayer.name}")
    private String name;
    /**
     * @Value - внедряет знчение в поле volume из файла musicPlayer13_3.properties
     */
    @Value("${musicPlayer.volume}")
    private int volume;

    public String getName() {
        return name;
    }

    public int getVolume() {
        return volume;
    }

    /**
     * Конструктор с параметром - лист музыки
     * @param musicList - жанр музыки RockMusic с id="musicBean"
     *
     * Внедрение листа музыки при помощи Конструктора
     *
     * Внедрить Бин-список в Бин-MusicPlayer
     *
     */

    //список жанров
    private List<Music> musicList;

    public MusicPlayer(List<Music> musicList) {
        this.musicList = musicList;
    }

    /**
     * В методе playMusic() будем получать песню
     * @return
     */
    public String playMusic(){
        Random random = new Random();
        return "Playing: " + musicList.get(random.nextInt(musicList.size())).getSong() + " with volume " + this.volume;

    }
}
