package Lesson8.org.example;

/**
 * class ClassicalMusic реализует интерфейс Music
 */
public class ClassicalMusic implements Music {
    /**
     * Создать приватный конструктор
     * Чтобы запретить создавать новые объекты при помощи ключевого слова new
     */
    private ClassicalMusic() {
        //запрещает создавать новые объекты при помощи ключевого слова new
    }

    /**
     * Создать Factory-method для создания объектов
     * Отмечен в фйле applicationContext8.xml, как
     * factory-method="getClassicalMusic"
     *
     * В данном случае Factory-method getClassicalMusic() создает объект один раз
     * и затем всегда использует этот созданный объект, так происходит потому, что
     * в фйле applicationContext8.xml используется паттерн scope="singleton" по умолчанию(не явным образом)
     */
    public static ClassicalMusic getClassicalMusic(){
        //создает объект класса ClassicalMusic
        return new ClassicalMusic();
    }

    /**
     * Init Method
     * Отмечен в фйле applicationContext8.xml, как
     * init-method="doMyInit"
     */
    public void doMyInit(){
        System.out.println("Doing my initialization");

    }

    /**
     * Destroy Method
     * Отмечен в фйле applicationContext8.xml, как
     * destroy-method="doMyDestroy"
     */
    public void doMyDestroy(){
        System.out.println("Doing my destruction");

    }

    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        return "Hungarian Rhapsody";
    }
}
