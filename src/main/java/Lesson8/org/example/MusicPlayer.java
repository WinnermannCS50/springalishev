package Lesson8.org.example;

/**
 * class MusicPlayer зависит от музыки
 */
public class MusicPlayer {


    //поле interface Music
    private Music music;

    //Название нашего плейера
    private String name;
    //Громкость
    private int volume;

    /**
     * Геттеры и Сеттеры для name и volume
     * @return
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    /**
     * Инверсия управления
     */

    /**
     * Внедрение зависимости при помощи Сеттера
     * @param music - параметр жанр музыки
     */
    public void setMusic(Music music) {
        this.music = music;
    }

    /**
     * В методе playMusic() будем получать песню
     */
    public void playMusic(){
        System.out.println("Playing: " + music.getSong());

    }
}
