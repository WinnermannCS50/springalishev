package Lesson8.org.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Пример паттерна Singleton
 * https://github.com/NeilAlishev/SpringCourse/blob/master/Lesson7Singleton.java
 */

public class TestSpring {
    public static void main(String[] args) {
        /**
         * ClassPathXmlApplicationContext - это специальный класс Спринга
         * Доступ к классу появляется благодаря зависимости spring-context
         * Данный класс собирает все Бины из applicationContext.xml по их id и ложит в
         * context = new ClassPathXmlApplicationContext("applicationContext8.xml");
         */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext8.xml");

        /**
         * Создать объект Бина ClassicalMusic
         */
        ClassicalMusic classicalMusic = context.getBean("musicBean", ClassicalMusic.class);
        /**
         * Вызвть метод getSong() класса ClassicalMusic
         */
        System.out.println(classicalMusic.getSong());

        /**
         * закрыть context
         */
        context.close();
    }
}
