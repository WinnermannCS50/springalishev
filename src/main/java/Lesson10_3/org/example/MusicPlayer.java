package Lesson10_3.org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * class MusicPlayer зависит от музыки
 *
 * @Component - создает Бин класса MusicPlayer
 */
@Component
public class MusicPlayer {


    //поле интерфейс Music
    private Music music;

    /**
     * Инверсия управления
     */

    /**
     * Внедрение зависимости при помощи Сеттера
     * @param music - жанр музыки
     *
     * @Autowired - внедряет подходящий Бин в Сеттер setMusic
     */
    @Autowired
    public void setMusic(Music music) {
        this.music = music;
    }

    /**
     * В методе playMusic() будем получать песню
     */
    public void playMusic(){
        System.out.println("Playing: " + music.getSong());

    }
}
