package Lesson12.org.example;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * class ClassicalMusic реализует интерфейс Music
 *
 * @Component - создает Бин класса ClassicalMusic
 *
 * @Scope("singleton") - Бин ClassicalMusic будет со скоупом singleton
 */
@Component
@Scope("singleton")
public class ClassicalMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    @Override
    public String getSong() {
        return "Hungarian Rhapsody";
    }
}
