package Lesson12.org.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        /**
         * ClassPathXmlApplicationContext - это специальный класс Спринга
         * Доступ к классу появляется благодаря зависимости spring-context
         * Данный класс собирает все Бины из applicationContext.xml по их id и ложит в
         * context = new ClassPathXmlApplicationContext("applicationContext12.xml");
         */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext12.xml");

        /**
         * Зесь указываем id="musicPlayer" и имя класса(интерфейса) Бин которого мы хотим получить MusicPlayer.class
         *
         * достаем Bean из applicationContext12.xml по его id="computer", вызываем через метод getBean()
         * используем два аргумента id="musicPlayer" и объект какого класса(интерфейса) хотим получить MusicPlayer.class
         * Объект класса context.getBean("musicPlayer", MusicPlayer.class)
         */
        MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);
        System.out.println(musicPlayer.getName());
        System.out.println(musicPlayer.getVolume());

        /**
         * Создадим два объекта класса ClassicalMusic
         * Если объекты идентичны(true), то значит scope Singleton
         */
        ClassicalMusic classicalMusic1 = context.getBean("classicalMusic", ClassicalMusic.class);
        ClassicalMusic classicalMusic2 = context.getBean("classicalMusic", ClassicalMusic.class);
        System.out.println(classicalMusic1==classicalMusic2);

        /**
         * закрыть context
         */
        context.close();
    }
}
