package Lesson10_6.org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * class MusicPlayer зависит от музыки
 *
 * @Component - создает Бин класса MusicPlayer
 */
@Component
public class MusicPlayer {

    //поле класс ClassicalMusic
    private ClassicalMusic classicalMusic;
    //поле класс RockMusic
    private RockMusic rockMusic;

    /**
     * Внедрение двух зависимостей при помощи Конструктора
     *
     * @Autowired - внедряет подходящие Бины в Конструктор MusicPlayer
     */
    @Autowired
    public MusicPlayer(ClassicalMusic classicalMusic, RockMusic rockMusic) {
        this.classicalMusic = classicalMusic;
        this.rockMusic = rockMusic;
    }

    /**
     * В методе playMusic() будем получать песню
     * @return
     */
    public String playMusic(){
        return "Playing: " + classicalMusic.getSong();

    }
}
