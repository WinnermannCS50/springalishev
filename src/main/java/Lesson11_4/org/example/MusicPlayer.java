package Lesson11_4.org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * class MusicPlayer зависит от музыки
 *
 * @Component - создает Бин класса MusicPlayer
 */
@Component
public class MusicPlayer {

    /**
     * Конструктор с параметром - жанр музыки
     * @param music1 - жанр музыки RockMusic с id="musicBean"
     * @param music2 - жанр музыки ClassicalMusic с id="classicalMusic"
     *
     * Внедрение двух зависимостей при помощи Конструктора
     *
     * @Autowired - внедряет подходящий Бин в Поле private Music music1
     * @Autowired - внедряет подходящий Бин в Поле private Music music2
     *
     * @Qualifier("musicBean") - уточняет какой именно Бин будет внедрен в зависимость music1 через Конструктор
     * @Qualifier("classicalMusic") - уточняет какой именно Бин будет внедрен в зависимость music2 через Конструктор
     */
    private ClassicalMusic classicalMusic;
    private RockMusic rockMusic;

    @Autowired
    public MusicPlayer(ClassicalMusic classicalMusic, RockMusic rockMusic) {
        this.classicalMusic = classicalMusic;
        this.rockMusic = rockMusic;
    }

    /**
     * В методе playMusic() будем получать песню
     */
    public void playMusic(MusicStyle style){
        Random random = new Random();

        // случайное целое число между 0 и 2
        int randomNumber = random.nextInt(3);

        if (style == MusicStyle.CLASSICAL){
            // случайная классическая песня
            System.out.println(classicalMusic.getSong().get(randomNumber));
        }else {
            // случайная рок песня
            System.out.println(rockMusic.getSong().get(randomNumber));
        }
    }
}
