package Lesson11_4.org.example;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * class RockMusic реализует интерфейс Music
 *
 * @Component("musicBean") - этой аннотацией помечен класс Бин которого создаст Spring
 * musicBean - это id Бина, если в скобках id Бина не указать, то id будет имя класса с маленькой буквы
 * в нашем случае rockMusic
 */
@Component("musicBean")
public class RockMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    private List<String> songs = new ArrayList<>();

    // Блок инициализации объекта (англ. Instance initialization block)
    // Выполняется каждый раз, когда создается объект класса
    {
        songs.add("Wind cries Mary");
        songs.add("Paint it black");
        songs.add("Can't seem to make you mine");
    }
    @Override
    public List<String> getSong() {
        return songs;
    }
}
