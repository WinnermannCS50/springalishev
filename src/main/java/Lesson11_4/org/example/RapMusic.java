package Lesson11_4.org.example;

import java.util.ArrayList;
import java.util.List;

/**
 * class RapMusic реализует интерфейс Music
 */
public class RapMusic implements Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    private List<String> songs = new ArrayList<>();

    // Блок инициализации объекта (англ. Instance initialization block)
    // Выполняется каждый раз, когда создается объект класса
    {
        songs.add("Beautiful Pain");
        songs.add("Solid");
        songs.add("I Need a Doctor");
    }

    @Override
    public List<String> getSong() {
        return songs;
    }
}
