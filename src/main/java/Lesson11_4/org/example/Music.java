package Lesson11_4.org.example;

import java.util.List;

/**
 * Все жанры музыки будут реализовывать интерфейс Music
 */
public interface Music {
    /**
     * Все жанры музыки обязаны иметь метод getSong()
     * @return
     */
    // Интерфейс тоже поменяли
    List<String> getSong();
}
