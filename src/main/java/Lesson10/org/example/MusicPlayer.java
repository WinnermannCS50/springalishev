package Lesson10.org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * class MusicPlayer зависит от музыки
 *
 * @Component - создает Бин класса MusicPlayer
 */
@Component
public class MusicPlayer {


    //поле класс ClassicalMusic
    private ClassicalMusic classicalMusic;

    /**
     * Инверсия управления
     */

    /**
     * Конструктор с параметром - классическая музыка
     * @param classicalMusic
     *
     * @Autowired - внедряет подходящий Бин в конструктор MusicPlayer
     */
    @Autowired
    public MusicPlayer(ClassicalMusic classicalMusic) {
        this.classicalMusic = classicalMusic;
    }

    /**
     * В методе playMusic() будем получать песню
     */
    public void playMusic(){
        System.out.println("Playing: " + classicalMusic.getSong());

    }
}
