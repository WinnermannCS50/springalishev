package Lesson10_4.org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * class MusicPlayer зависит от музыки
 *
 * @Component - создает Бин класса MusicPlayer
 */
@Component
public class MusicPlayer {

    /**
     * поле интерфейс Music
     *
     * Внедрение зависимости при помощи Поля
     *
     * @Autowired - внедряет подходящий Бин в Поле private Music music
     */
    @Autowired
    private Music music;

    /**
     * В методе playMusic() будем получать песню
     */
    public void playMusic(){
        System.out.println("Playing: " + music.getSong());

    }
}
