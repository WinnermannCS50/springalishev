package Lesson7.org.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Пример паттерна Singleton
 * https://github.com/NeilAlishev/SpringCourse/blob/master/Lesson7Singleton.java
 */

public class TestSpring {
    public static void main(String[] args) {
        /**
         * ClassPathXmlApplicationContext - это специальный класс Спринга
         * Доступ к классу появляется благодаря зависимости spring-context
         * Данный класс собирает все Бины из applicationContext.xml по их id и ложит в
         * context = new ClassPathXmlApplicationContext("applicationContext7.xml");
         */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext7.xml");

        /**
         * Создание двух Бинов (Объектов) с применением Паттерна scope="singleton"
         * scope="singleton" - указывать явным образом в фойле applicationContext7.xml не требуется, применяется по умолчанию
         */
        MusicPlayer firstMusicPlayer = context.getBean("musicPlayer", MusicPlayer.class);

        MusicPlayer secondMusicPlayer = context.getBean("musicPlayer", MusicPlayer.class);

        /**
         * Проверить указывают ли обе переменные (firstMusicPlayer и secondMusicPlayer) на один и тот же участок памяти
         * Проверить равны ли объекты
         * true - будет означать, что обе переменные указывают(ссылаются) на один и тот же объект
         * одинаковый хэш - тоже подтверждает что обе переменные указывают(ссылаются) на один и тот же объект
         */
        boolean comparison = firstMusicPlayer == secondMusicPlayer;
        System.out.println(comparison);
        System.out.println(firstMusicPlayer);
        System.out.println(secondMusicPlayer);

        /**
         * Изменить значение переменной volume у плейера firstMusicPlayer
         * При установке параметра volume = 10 для firstMusicPlayer, параметр volume становится
         * равным 10 и для secondMusicPlayer
         * Это происходит потому, что обе переменные (firstMusicPlayer и secondMusicPlayer) ссылаются на один и тот же объект
         * По умолчанию применяется паттерн Singleton
         */
        firstMusicPlayer.setVolume(10);
        System.out.println(firstMusicPlayer.getVolume() + " volume для firstMusicPlayer");
        System.out.println(secondMusicPlayer.getVolume() + " volume для secondMusicPlayer");

        /**
         * закрыть context
         */
        context.close();
    }
}
